# Genetic Algorithm to synthesise voice data
# Created by Sam Kennedy on Aug 19th 2017
# I was really bored

# Imports
from gene import Gene
import copy
import sys

# Create a gene
gene = Gene()
gene.read_wav("dwight.wav")
gene.initial_gen()

# Loop
best_score = gene.score()
output_ctr = 1

while True:
    # Mutate
    score_delta = gene.mutate(1000, 16000)

    # If improved keep, otherwise restore backup
    if score_delta < 0:
        print "Generation " + str(output_ctr) + " Best score: " + str(best_score)
        best_score += score_delta
        if output_ctr % 10000 == 0:
            gene.output(output_ctr)
        output_ctr += 1
    else:
        gene.restore()


