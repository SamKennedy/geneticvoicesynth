import random
import wave
import struct


class Gene(object):
    num_frames = 0
    target_sequence = []
    restore_map = {}

    def __init__(self):
        self.bytes = []

    def initial_gen(self):
        for i in range(0, Gene.num_frames):
            self.bytes.append(random.randint(0, 65535))

    def score(self):
        if len(self.bytes) == len(Gene.target_sequence):
            score = 0
            for i in range(0, Gene.num_frames):
                t_int = Gene.target_sequence[i]
                c_int = self.bytes[i]
                dif = t_int - c_int
                score += dif * dif

            return score

    def output(self, file_no):
        sound_output = wave.open('out/noise' + str(file_no) + '.wav', 'w')
        sound_output.setparams((2, 2, 48000, 0, 'NONE', 'not compressed'))

        values = []

        for i in range(0, Gene.num_frames):
            packed_value = struct.pack('h', self.bytes[i] - 32768)
            values.append(packed_value)
            values.append(packed_value)

        value_str = ''.join(values)
        sound_output.writeframes(value_str)
        sound_output.close()

    def mutate(self, num_its, max_delta):
        # Reset restore map
        Gene.restore_map = {}

        # Spin a "spinner"
        spinner = random.randint(1, 100)

        # Score delta is how much the score has changed
        score_delta = 0
        for i in range(0, spinner):
            # Pick a random allele
            allele_num = random.randint(0, Gene.num_frames - 1)

            # Back it up in the restore_map
            if allele_num not in Gene.restore_map:
                Gene.restore_map[allele_num] = self.bytes[allele_num]

            # Calculate this allele's score before mutating
            pre_mutate_score = (Gene.target_sequence[allele_num] - self.bytes[allele_num])**2

            # Mutate it by +/- max_delta
            self.bytes[allele_num] += random.randint(0, 2 * max_delta) - max_delta

            # Calculate post mutation score
            post_mutate_score = (Gene.target_sequence[allele_num] - self.bytes[allele_num])**2

            # Fix if out of bounds
            if self.bytes[allele_num] < 0:
                self.bytes[allele_num] = 0
            elif self.bytes[allele_num] > 65535:
                self.bytes[allele_num] = 65535

            # Update score delta
            score_delta += post_mutate_score - pre_mutate_score

            return score_delta

    def restore(self):
        for index in Gene.restore_map:
            self.bytes[index] = Gene.restore_map[index]

    @staticmethod
    def read_wav(path):
        # Open the file, get the number of frames and the target sequence, save them, then close the file
        wave_handle = wave.open(path)
        Gene.num_frames = wave_handle.getnframes()
        frames = wave_handle.readframes(Gene.num_frames)

        Gene.target_sequence = []
        for i in range(0, Gene.num_frames * 2, 2):
            Gene.target_sequence.append(int((frames[i] + frames[i+1]).encode('hex'), 16))

        wave_handle.close()
